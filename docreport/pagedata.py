# -*- coding: utf-8 -*-

from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import mm  #  mm = 2.834645669291339
from reportlab.lib.pagesizes import A4
from .filters import Filter
from reportlab.lib.enums import TA_LEFT,TA_RIGHT, TA_CENTER

#page_xlen, page_ylen = A4
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib.fonts import addMapping
import os


# ARIAL serve perché alcuni char non vengono visualizzati: example1: kŞk bās
#  https://stackoverflow.com/questions/45001829/reportlab-unicode-characters-appear-as-boxes-in-unicode-supported-font
# quindi styleN non avrà questo problema

# il font di default è Helvetica size 12

from reportlab import rl_config

fontspath = os.path.join(os.path.dirname(__file__), 'font')
#rl_config.TTFSearchPath.append(fontspath)

for fontname in os.listdir(fontspath):
    if fontname.lower().endswith('ttf'):

        fontfile =os.path.join(fontspath, fontname)
        assert os.path.isfile(fontfile)

        name = os.path.splitext(fontname.replace("-",""))[0]
        pdfmetrics.registerFont(TTFont(name, fontfile))

        print ("Reportlab adding font %s"%name)
        #addMapping(fontname, 0, 0, fontname)
#addMapping(face, bold, italic, psname):
#addMapping("arial", "Arialbd", "Ariali", "arial"):

addMapping("arial", 0, 0, "Arial")  # normal
addMapping("arial", 0, 1, "Ariali")  # italic
addMapping("arial", 1, 0, "Arialbd")  # bold
addMapping("arial", 1, 1, "Arialbi")  # italic and bold


from reportlab.lib import styles as ss
from reportlab import rl_config
rl_config.canvas_basefontname = 'Arial'
#ctx.canvas.setFont("RobotoBlack", 12)
ss._baseFontName = 'Arial'



styles = getSampleStyleSheet()


styleN = styles['Normal'].clone(name='NormalRight', alignment=TA_LEFT) #, fontName='arial')

# Title, OrderedList Normal Italic Heading6.. Heading1  Definition  Code Bullet  BodyText
#styleN = styles['Normal']
styleH = styles['Heading1']



# default_context = dict(
#     page_width = A4[0],
#     page_height = A4[1],
#     #unit=mm,
#     filter_cls=Filter,
#     locale='en-DK',
#     canvas=None,
# )