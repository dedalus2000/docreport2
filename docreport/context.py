# -*- coding: utf-8 -*-
from .pagedata import styleN
from .pagedata import mm, A4
from .filters import Filter
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.colors import red, blue, Color
from contextlib import contextmanager







class addStyle(object):
    def __init__(self, **styles):
        self.styles = styles

def _extract_style_data(ss, otherstyles=None):
    if otherstyles is None:
        otherstyles = {}

    dd = []
    for nn in ParagraphStyle.defaults:
        if nn in otherstyles:
            dd.append(otherstyles[nn])
        else:
            dd.append(getattr(ss, nn))
    return tuple(dd)  # list is not hashable


class Context(object):
    _vars = (
        "canvas", "border", "height", "width", "style", "styles", "border_cb",
        "cols_widths", "cols_styles", "y_padding", "x_padding", "rows_start_x",
        "rows_end_x", "rows_start_y", "rows_end_y", "page_height", "page_width","escape",
        "page_left_border", "page_right_border", "page_top_border", "page_bottom_border"
    )

    canvas=None
    border=None
    height=None
    width=None
    style=styleN
    styles={}
    border_cb=None
    cols_widths=None
    cols_styles=None
    y_padding=None
    x_padding=None
    rows_start_x = 3*mm

    rows_end_x=None
    rows_start_y=None
    rows_end_y=None
    page_height=None
    page_width=None
    escape = Filter()  # escape filter instance: ex. Filter()

    # nota: la stampa da browser aggiunge in genere 0,17" di margine (0.45mm). TargeCross anche di più
    page_left_border=6*mm
    page_right_border=6*mm
    page_top_border=6*mm
    page_bottom_border=6*mm




    def __init__(self, **kwargs):
        for kk,vv in kwargs.items():
            if kk in self._vars:
                setattr(self, kk, vv)

    # nota: getitem e setitem da fare
    def __setattr__(self, name, value):
        if name=='style':
            if isinstance(value, addStyle):
                dd = _extract_style_data(self.style, otherstyles=value.styles)
                if not dd in self.styles:
                    # prima volta che usiamo questo stile
                    newstyle = ParagraphStyle(self.style.name + "C", self.style)
                    self.styles[dd] = newstyle
                    for kk,vv in value.styles.items():
                        setattr(newstyle, kk, vv)
                value = self.styles[dd]
            else:
                # ParagraphStyle
                dd = _extract_style_data(value)
                if not dd in self.styles:
                    self.styles[dd] = value
        super(Context, self).__setattr__(name, value)

    # def __getattr__(self, name):
    #     assert name in _vars
    #     return self[name]
#      def _manage_styles(self, adds):

    def _print(self):
        for name in self._vars:
            print ("> %s : %s" % (name, getattr(self, name, "[ERR]")))

    def clone(self, **kwargs):
        assert set(kwargs).issubset(self._vars)

        obj = self.__class__()
        for kk in self._vars:
            if kk in kwargs:
                setattr(obj, kk, kwargs[kk])
            else:
                try:
                    setattr(obj, kk, getattr(self, kk))
                except AttributeError as err:  # è una property
                    print ("Skipping ctx", kk, err)
        return obj

    @contextmanager
    def up(self, **kwargs):
        assert set(kwargs).issubset(self._vars)

        dd = {}
        for kk,vv in kwargs.items():
            dd[kk] = getattr(self, kk, vv)
            setattr(self, kk, vv)

        yield
        for kk,vv in dd.items():
            setattr(self, kk, vv)


# # aggiungo gli stili del testo
# for sname, svalue in ParagraphStyle.defaults.items():
#     sname = "s_%s"%sname
#     _vars.append(sname)
#     setattr(Context, sname, svalue)
    # @property
    # def rows_height(self):
    #     return self.rows_end_y - self.rows_start_y

class ContextA4(Context):
    #rows_end_x = A4[0]-rows_start_x
    # @property
    # def rows_end_x(self):
    #     if self.cols_widths:
    #         return sum(self.cols_widths) + self.rows_start_x
    #     return self.page_width - self.rows_start_x

    rows_start_y = 50*mm
    rows_end_y = A4[1] - 40*mm
    rows_end_x = A4[0] - Context.page_right_border

    page_width, page_height = A4

    y_padding = 0
    x_padding = 0

print ("AAA", ContextA4.rows_end_x)
