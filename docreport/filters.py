# encoding: utf-8

from babel import dates
from babel import numbers
from markupsafe import escape as markup_escape
import datetime
import decimal
import sys


# TODO
#  &nbsp per gli spazi iniziali di tutte le righe e anche interni :-(

# TO BE IMPROVED
# def _get_price_filter(zero=2):
#     u"""Convert Decimal to a money formatted string.
#        No round is done, please provide it by your own
#     """
#     def prices(value):
#         if value is None or value== '':
#             return ''
#         # lo zero lo gestisce in modo più consono
#         return format_decimal(value, format='#,##0.' + ('0'*zero) + (("#"*10 ) if value else ''), locale=context['locale'])
#     return prices

# P = _get_price_filter(zero=2)

# def get_strip_filter(depth=2):
#     def strip_filter(value, ctx):
#         if value is None or value=='':
#             return ''
#         return format_decimal(value, format='0.' + "0"*depth + "##", locale=ctx['lang'])
#     return strip_filter


class EmptyFilter(object):
    def __call__(self, txt):
        return str(txt)


unary = ['br']  # l'unico "unario" che conosco

class Tag(object):
    children = None  # children sarà sempre una lista
    name = None
    attributes = None

    def __call__(self, **attributes):
        tag = Tag()
        tag.name = self.name
        tag.attributes = attributes
        tag.children = []
        return tag

    def __getitem__(self, children):
        tag = Tag()
        tag.name = self.name
        tag.attributes = self.attributes
        if tag.name in unary:
            raise Exception("Tag '{}' cannot have children".format(tag.name))

        # children sarà sempre una lista
        if not isinstance(children, (tuple, list)):
            children = [children]
        tag.children = children
        return tag

    def __getattr__(self, name):
        t = Tag()
        t.name = name
        return t

    def toXml(self, escape=markup_escape):
        tt = []
        attrs = ''
        if self.attributes:
            attrs = " %s " % ' '.join([ '''{}="{}"'''.format(k,v) for k,v in self.attributes.items() ])

        if self.name=='N':
            escape = None
        elif self.name in unary:
            tt.append('<{}{}/>'.format(self.name, attrs))
        elif self.name:
            tt.append("<{}{}>".format(self.name, attrs))

        for child in self.children or []:

            if isinstance(child, Tag):  # ricorsivo
                tt.append(child.toXml(escape=escape))
            elif isinstance(child, (list, tuple)):  # ricorsivo
                tt.extend(T[child].toXml(escape=escape))

            elif child is None:  # Se è None so già cosa fare..
                tt.append('')

            elif escape:         # .. negli altri casi "escapo"
                tt.append(escape(child).replace(' ', '&nbsp;'))
            else:
                tt.append(child)  # ci deve pensare l'utente a "escapare"

        if self.name=='N':
            pass
        elif self.name and self.name not in unary:
            tt.append("</{}>".format(self.name))
        return ''.join(tt)

    # def __str__(self):
    #     return self.toXml(Filter())
    # __repr__ = __str__
    # __html__ = __str__

T = Tag()


class Filter(object):
    def __init__(self, locale='en-DK', date_format='long'):
        self.locale = locale.replace('-', '_')  # NOTA IMPORTANTE se vuoi settare self.locale a mano..
        self.date_format = date_format

    def __call__(self, txt):
        return self.default_filter(txt)

    def f_date(self, obj):
        return dates.format_date(obj, format=self.date_format, locale=self.locale)

    def f_datetime(self, obj):
        # if obj.tzinfo:
        #     return u"[TO BE DONE]"
        # dt = dates.format_time(obj, "hh:mm a", locale=thread_local.context.get('locale', 'en_DE'))
        txtdt = dates.format_time(obj, "HH:mm a", locale=self.locale)
        txtdd = self.f_date(obj)
        return u'%s  %s' % (txtdd, txtdt)

    def f_decimal(self, txt):
        if txt is None:
            return ''
        return numbers.format_decimal(txt, locale=self.locale)

    def f_text(self, txt):
        # gestisce le str
        # trasforma i \n in br, e gli spazi in &nbsp
        #

        ntxt = []
        # def _addNBSP(t):
        #     if t and t[0]==' ':
        #         ntxt.append('&nbsp;')
        #         _addNBSP(t[1:])
        #     else:
        #         ntxt.append(t)
        for part in txt.split('\n'):
            part = markup_escape(part)
            ntxt.append(part)
            ntxt.append('<br/>')
        if ntxt:
            ntxt.pop() # l'ultimo br è fittizio
        return ''.join(ntxt)

    def default_filter(self, txt):
        # Filter
        if txt is None:
            return ''
        if isinstance(txt, datetime.datetime):
            return self.f_datetime(txt)
            #return txt.strftime('%X, %d %b %Y')
        if isinstance(txt, datetime.date):
            return self.f_date(txt)
            #return txt.strftime('%d %b %Y')
        if isinstance(txt, (decimal.Decimal, float, int)):
            # perche' i decimal arrivano anche 7 zeri dopo il punto, quindi li tratto a parte
            return self.f_decimal(txt)
        if isinstance(txt, LocalDecimal):
            return numbers.format_decimal(
                txt.val, format=txt.format, locale=self.locale)
        if isinstance(txt, WeekYear):
            return txt.week
        if sys.version_info[0] < 3:
            # esegue l'escape
            # a sinistra aggiunge &nbsp; al posto degli spazi iniziali
            # sostituesce i \n con <br/>
            if isinstance(txt, (str, unicode)):   # py3 error
                return self.f_text(txt)
        elif isinstance(txt, str):
            return self.f_text(txt)
        print("Warning %s not managed" % type(txt))
        return str(txt)


class CustomFormat(object):
    """
    Astrazione per definire formattazioni personalizzate via Filter

    Args:
        val (obj): oggetto da formattare
    """
    def __init__(self, val):
        self.val = val


class LocalDecimal(CustomFormat):
    """
    (per uso con Filter)

    Formatta un numero con separatore migliaia e decimali localizzato

    Args:
        val (number): numero da formattare
        zeroes (bool): se aggiungere o meno gli zeri ad un decimali
    """
    TRAILING_ZEROES = '#,##0.00;-#'
    NO_TRALING_ZEROES = '#,##0.##;-#'

    def __init__(self, val, zeroes=True):
        super(LocalDecimal, self).__init__(val)

        if zeroes:
            self.format = LocalDecimal.TRAILING_ZEROES
        else:
            self.format = LocalDecimal.NO_TRALING_ZEROES

    def __add__(self, other):
        if type(other) is LocalDecimal:
            return LocalDecimal(self.val + other.val)
        return LocalDecimal(self.val + decimal.Decimal(other))

    def __iadd__(self, other):
        if type(other) is LocalDecimal:
            self.val += other.val
        else:
            self.val += decimal.Decimal(other)
        return self

    def __sub__(self, other):
        if type(other) is LocalDecimal:
            return LocalDecimal(self.val - other.val)
        return LocalDecimal(self.val - decimal.Decimal(other))

    def __mul__(self, other):
        if type(other) is LocalDecimal:
            return LocalDecimal(self.val * other.val)
        return LocalDecimal(self.val * decimal.Decimal(other))

class WeekYear(CustomFormat):
    """
    (peer uso con Filteer)

    Formatta una data nel formato W42/2020

    Args:
        val (date): data da formattare
    """
    def __init__(self, val):
        super(WeekYear, self).__init__(val)

        try:
            self.week = self.val.strftime('W%V/%Y')
        except Exception as e:
            print('oggetto {} non convertibile in data: {}'.format(
                self.val, e))
            self.week = ''



if __name__ == '__main__':
    ff = Filter(locale='en-US', date_format='medium')  # locale='en-DK', date_format='long'):
    print ff(decimal.Decimal(5523.02))
    print ff(5523.100000)
    print ff(datetime.date.today())
    print
    ff = Filter(locale='it-IT', date_format='medium')  # locale='en-DK', date_format='long'):
    print ff(decimal.Decimal(5523.02))
    print ff(5523.100000)
    print ff(datetime.date.today())
