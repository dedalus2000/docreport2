# -*- coding: utf-8 -*-

def borderbox(canvas, x,y, obj):
    canvas.rect(x,y, obj.width,obj.height)

def getRborderbox(radius):
    def rborderbox(canvas, x,y, obj):
        canvas.roundRect(x,y, obj.width,obj.height, radius)
    return rborderbox

def vborders(canvas, x,y, hobj, skip=None):
    skip = set(skip or [])

    # prima riga a sinistra
    if 0 not in skip:
        canvas.line(x, y,
                    x, y+hobj.height)

    if getattr(hobj, "paths", None) and hobj.paths:
        for ii, path in enumerate(hobj.paths):
            if (ii+1) not in skip:
                # riga a destra per l'oggetto corrente
                canvas.line(x+path.x+path.width, y,
                            x+path.x+path.width, y+hobj.height)
    else:
        if 1 not in skip:
            # nota: se hobj.paths è vuoto qui width è zero e sovrascrive
            canvas.line(x+hobj.width, y,
                        x+hobj.width, y+hobj.height)



class RowsBorders(object):
    def __init__(self, hskip=None, vskip=None):
        self.hskip = set(hskip or [])
        self.vskip = set(vskip or [])

    def draw(self, canvas, x,y, obj, first_row):
        # righe orizzontali
        if first_row:
            hborders(canvas, x,y, obj, skip=self.hskip)

        hborders(canvas, x,y+obj.height, obj, skip=self.hskip)

        # righe verticali
        vborders(canvas, x,y, obj, skip=self.vskip)


def hborders(canvas, x,y, vobj, skip=None):
    skip = set(skip or [])

    if getattr(vobj, "paths", None) and vobj.paths:
        for ii, path in enumerate(vobj.paths):
            if ii not in skip:
                # riga in basso per l'oggetto corrente
                canvas.line(x+path.x,            y,
                            x+path.x+path.width, y)

    else:
        if 0 not in skip:
            canvas.line(x,            y,
                        x+vobj.width, y)


def table_borders(canvas, x,y, vobj):
    canvas.line(x,y, x+vobj.width,y)
    #canvas.line(x,y+vobj.height, x+vobj.width,y+vobj.height)
    cury = y
    if getattr(vobj, "paths", None):
        for path in vobj.paths:
            hborders(canvas, x,cury, path.wrappable)
            canvas.line(x,y+path.y+path.height, x+vobj.width,y+path.y+path.height)
            cury += path.height

class Generic(object):
    def __init__(self, canvas, x,y, obj):
        pass