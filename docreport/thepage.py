# -*- coding: utf-8 -*-

from .wrapobjs import WrappableInterface, Composition, Text, _Path
from .localreportlab import MyCanvas
from .pagedata import mm, A4
from .filters import Filter
from .context import ContextA4


# class RowsUtil(Composition):
#     # a VerticalWrap. filled with Horiz.Wrap.
#     #
#     cols_widths = None
#     cols_styles = None

#     def __init__(self, ctx):
#         # in kwargs c'è -opzionale- border_cb
#         self.ctx = ctx
#         # self.cols_widths = kwargs.pop('cols_widths', None)
#         # self.cols_styles = kwargs.pop('cols_styles', None)
#         super(RowsUtil, self).__init__(ctx=self.ctx)

#     def _beforeAddingPath(self, path):
#         if self.paths:
#             path.idx = self.paths[-1].idx + 1
#             path.height_sum = self.paths[-1].height_sum + path.height
#         else:
#             path.idx = 0
#             path.height_sum = path.height

#     # def vAdd(self, row):
#     #     assert isinstance(row, WrappableInterface)
#     #     if self.ctx.rows_height and row.height>self.ctx.rows_height:
#     #         print ("Warning: che faccio? riga maggiore dello spazio pagina %s>%s"%(row.height, self.ctx.rows_frame_height))
#     #     return super(RowsUtil, self).vAdd(row)

#     def add(self, row):
#         raise Exception('Dont use me')

#     def hAdd(self, row):
#         raise Exception('Dont use me')

#     def _cloneEmpty(self):
#         return RowsUtil(ctx=self.ctx)

#     def addRowValues(self, fields): #, cols_widths=None, cols_styles=None):
#         cols_widths = self.ctx.cols_widths
#         if not cols_widths: # is False:
#             cols_widths = [None]*len(fields)

#         # cols_widths = cols_widths or self.cols_widths or self.ctx.cols_widths
#         # cols_styles = cols_styles or self.cols_styles or self.ctx.cols_styles
#         cols_styles = self.ctx.cols_styles
#         if cols_styles is None:
#             cols_styles = [self.ctx.style] * len(fields)
#         assert isinstance(cols_widths, (tuple,list))
#         assert isinstance(fields, (tuple,list))
#         assert len(cols_widths)==len(fields)==len(cols_styles)

#         hh = Composition(ctx=self.ctx)

#         # calcola il width rimasto in "single"
#         #  quando width richiesto è None
#         nnone = 0
#         xdim = self.width
#         for dd in cols_widths:
#             if dd is None:
#                 nnone += 1
#             else:
#                 xdim -= dd
#         single = xdim / nnone if nnone else 0

#         for field, width, style in zip(fields, cols_widths, cols_styles):
#             if style:
#                 print (33)
#             ww = Text(field, width if width is not None else single, ctx=self.ctx)
#             hh.hAdd(ww)
#         return self.vAdd(hh)

#     def splitByHeight(self, requested_height):
#         assert requested_height>0

#         rr_before = self._cloneEmpty()
#         rr_after = self._cloneEmpty()

#         for path in self.paths:
#             if path.height_sum<=requested_height:
#                 rr_before.vAdd(path.wrappable)
#             else:
#                 rr_after.vAdd(path.wrappable)
#         return rr_before, rr_after

#         # def drawOn(self, canvas, x, y):
#         #     cury = y
#         #     for row in self.wrappables:
#         #         row.drawOn(canvas, x, cury)
#         #         cury += row.height
#         #     canvas.lines([[x,y, self.width,y], [x,cury, self.width,cury]])



class MyPage(object):
    ctx = None #ContextA4()

    def manage_ctx(self, ctx):
        pass

    #@staticmethod

    # @property
    # def curpage_x(self):
    #     return self.page_width-20*mm

    # @property
    # def curpage_y(self):
    #     return self.page_height-15*mm

    curpage = None
    rows_cur_dy = 0

    rows_obj = None


    @property
    def canvas(self):
        return self.ctx.canvas

    def __init__(self, file_or_canvas, ctx=None, cols_widths=None, cols_styles=None):
        if isinstance(file_or_canvas, MyCanvas):
            self.ctx = file_or_canvas.ctx
            if ctx:
                raise Exception("Useless ctx")
        else:
            self.ctx = ctx or self.ctx or ContextA4()
            MyCanvas(file_or_canvas, ctx=self.ctx)  # associa self.ctx.canvas

        assert self.ctx
        assert self.ctx.canvas

        # let others to manage it owns ctx
        # dds = [getattr(x,"manage_ctx", None) for x in self.__class__.mro()[::-1]]
        # for dd in dds:
        #     if dd:
        #         dd(self, self.ctx)
        self.manage_ctx(self.ctx)

        self.ctx.cols_widths = cols_widths or self.ctx.cols_widths
        self.ctx.cols_styles = cols_styles or self.ctx.cols_styles

        self.curpage = 0

        finalizePages_fn = getattr(self, 'finalizePages', None)
        if finalizePages_fn:
            self.ctx.canvas.finalizePages = finalizePages_fn
        #self.initRows(rows=rows)


    # def initRows(self, rows=None):
    #     self.rows_obj = RowsUtil(ctx=self.ctx, **(rows or {}))

    def save(self):
        self.ctx.canvas.save()

    def new_page(self):
        self.ctx.canvas.showPage()
        self.curpage +=1
        self.rows_cur_dy = 0

    def on_init_page(self):
        pass
        # può attingere a self.curpage e self.data
        #print ("Pagina %s : rimangono %s"%(self.curpage, rows.height ))

        # self._line_hor(self.ctx.rows_start_y)
        # self._line_hor(self.ctx.rows_end_y)

    def _line_hor(self, yy):
        endx = self.ctx.rows_end_x
        if not endx:
            if not self.ctx.rows_obj:
                raise Exception('Missing rows')
            endx = self.ctx.rows_start_x + self.ctx.rows_obj.width
        self.ctx.canvas.line(self.ctx.rows_start_x, yy, endx, yy)

    ###
    def Text(self, *args, **kwargs):
        if not kwargs.get('ctx'):  # None o non esistente
            kwargs['ctx'] = self.ctx
        elif isinstance(kwargs['ctx'], dict):
            print (kwargs)
            kwargs['ctx'] = self.ctx.clone(**kwargs['ctx'])
        return Text(*args, **kwargs)

    def Composition(self, *args, **kwargs):
        if not kwargs.get('ctx'):  # None o non esistente
            kwargs['ctx'] = self.ctx
        elif isinstance(kwargs['ctx'], dict):
            kwargs['ctx'] = self.ctx.clone(**kwargs['ctx'])
        return Composition(*args, **kwargs)

    def addRowValues(self, fields, cols_widths=None, border_cb=None, cols_styles=None, rows_borders=None):
        if cols_widths is False:
            cols_widths = [None]*len(fields)

        cols_widths = cols_widths or self.ctx.cols_widths
        cols_styles = cols_styles or self.ctx.cols_styles
        if cols_styles is None:
            cols_styles = [self.ctx.style] * len(fields)


        assert isinstance(cols_widths, (tuple,list))
        assert isinstance(fields, (tuple,list))
        assert len(cols_widths)==len(fields)==len(cols_styles)

        hh = Composition(ctx=self.ctx)

        # calcola il width rimasto in "single"
        #  quando width richiesto è None
        nnone = 0
        xdim = self.ctx.rows_end_x - self.ctx.rows_start_x
        for dd in cols_widths:
            if dd is None:
                nnone += 1
            else:
                xdim -= dd
        single = xdim / nnone if nnone else 0

        for field, width, _style in zip(fields, cols_widths, cols_styles):
            ctx=self.ctx
            if _style:
                ctx = ctx.clone(style=_style)
            ww = Text(field, width if width is not None else single, ctx=ctx)
            hh.hAdd(ww)
        return self.addRow(hh, border_cb=border_cb, rows_borders=rows_borders)

    def addRow(self, obj, border_cb=None, rows_borders=None):
        cur_y = self.ctx.rows_start_y + self.rows_cur_dy

        if cur_y + obj.height>self.ctx.rows_end_y:
            # vado oltre! cambio pagina
            self.new_page()
            self.on_init_page()
            cur_y = self.ctx.rows_start_y+self.rows_cur_dy

        if border_cb:
            with self.ctx.up(border_cb=border_cb):
                obj.drawAt(self.ctx.rows_start_x, cur_y)
        else:
            obj.drawAt(self.ctx.rows_start_x, cur_y)

        if rows_borders:
            rows_borders.draw(self.ctx.canvas, self.ctx.rows_start_x, cur_y, obj, self.rows_cur_dy==0)

        self.rows_cur_dy += obj.height