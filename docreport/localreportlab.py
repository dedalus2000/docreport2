# -*- coding: utf-8 -*-

from reportlab.pdfgen.canvas import Canvas as _Canvas
from reportlab.platypus import Paragraph, Frame
from reportlab.platypus.flowables import KeepInFrame

#from .pagedata import default_context  # page_ylen, A4
from .filters import Filter, Tag, T
import copy


# class MyFrame(Frame):
#     ctx = None
#     def __init__(self, xx, yy, xlen, ylen, page_height, *args, **kwargs):
#         Frame.__init__(self, xx, page_height-yy-ylen, xlen, ylen, *args, **kwargs)


# to be done
class MyKeepInFrame(KeepInFrame):
    def __init__(self, ctx, *args, **kwargs):
        self.ctx = ctx

        self.canv = None  # bug?
        KeepInFrame.__init__(self, *args, **kwargs)

    def drawAt(self, x, y):
        KeepInFrame.drawOn(self, self.ctx.canvas, x, self.ctx.page_height-y)
        return self


class MyParagraph(Paragraph):
    page_height = None
    def __init__(self, text, ctx, **kwargs):
        self.ctx = ctx

        escape = self.ctx.escape #Filter()

        text = T[text]  # sempre e comunque

        if isinstance(text, Tag):
            text = text.toXml(escape)
        else:
            text = escape(text)
        Paragraph.__init__(self, text, self.ctx.style, **kwargs)

    def drawAt(self, x, y, _sW=0):
        Paragraph.drawOn(self, self.ctx.canvas, x, self.ctx.page_height-y, _sW)
        return self


class NumberedCanvas(_Canvas):
    # NO http://code.activestate.com/recipes/546511-page-x-of-y-with-reportlab/
    # https://gist.github.com/KanorUbu/5203608
    def __init__(self, *args, **kwargs):
        # kwargs['verbosity'] = 1
        _Canvas.__init__(self, *args, **kwargs)
        self._codes = []

    def _save_context(self):
        _formsinuse = copy.deepcopy(self._formsinuse)
        self._codes.append({'code': self._code, 'stack': self._codeStack,
                            'cM' : self._cropMarks,
                            '_pageRotation':self._pageRotation,
                            '_currentPageHasImages': self._currentPageHasImages,
                            '_pageTransition': self._pageTransition,
                            '_pageCompression': self._pageCompression,
                            '_pageDuration': self._pageDuration,
                            '_psCommandsBeforePage': self._psCommandsBeforePage,
                            '_preamble': self._preamble,
                            '_psCommandsAfterPage': self._psCommandsAfterPage,
                            '_onPage': self._onPage,
                            '_formsinuse': _formsinuse
                            })
    def showPage(self):
        self._save_context()
        self._startPage()

    def save(self):
        """add page info to each page (page x of y)"""
        self._save_context()
        # reset page counter
        self._pageNumber = 0
        for code in self._codes:
            # recall saved page
            self._code = code['code']
            self._codeStack = code['stack']
            self._cropMarks = code['cM']
            self._pageRotation = code['_pageRotation']
            self._currentPageHasImages = code['_currentPageHasImages']
            self._pageTransition = code['_pageTransition']
            self._pageCompression = code['_pageCompression']
            self._pageDuration = code['_pageDuration']
            self._psCommandsBeforePage = code['_psCommandsBeforePage']
            self._preamble = code['_preamble']
            self._psCommandsAfterPage =  code['_psCommandsAfterPage']
            self._onPage =  code['_onPage']
            self._formsinuse = code['_formsinuse']
            # self.setFont("Helvetica", 7)
            # self.drawRightString(200*mm, 15*mm,
            #     "Page %(this)i/%(total)i" % {
            #         'this': self._pageNumber+1,
            #         'total': len(self._codes),
            #     }
            # )
            finalizePages_fn = getattr(self, "finalizePages", None)
            if finalizePages_fn:
                finalizePages_fn(self._pageNumber+1, len(self._codes))
            _Canvas.showPage(self)
        self._doc.SaveToFile(self._filename, self)


    # def finalizePages(self, p_number, p_count):
    #     pass
        # self.setFont("Helvetica", 7)
        # self.drawRightString(self.ctx.page_width-40, 40,
        #     "Page %(this)i of %(total)i" % {
        #         'this': self._pageNumber+1,
        #         'total': len(self._codes),
        #     }
        # )


class MyCanvas(NumberedCanvas):
    ctx = None
    def __init__(self, filename, ctx, *args, **kwargs):
        assert ctx
        self.ctx = ctx
        if getattr(self.ctx, "canvas"):
            raise Exception("Context already linked to canvas")
        self.ctx.canvas = self

        NumberedCanvas.__init__(self, filename, (self.ctx.page_width, self.ctx.page_height), *args, **kwargs)

    def line(self, x1,y1, x2,y2):
        _Canvas.line(self, x1, self.ctx.page_height-y1, x2, self.ctx.page_height-y2)

    def roundRect(self, x, y, width, height, radius, stroke=1, fill=0):
        _Canvas.roundRect(self, x, self.ctx.page_height-y-height, width, height, radius, stroke, fill)

    def rect(self, xx, yy, xlen, ylen, **kwargs):
        # stroke=0, fill=1)
        # print xx/mm,yy/mm
        _Canvas.rect(self, xx, self.ctx.page_height-(yy), xlen, -ylen, **kwargs)

    def lines(self, linelist):
        newlinelist = []
        for (x1,y1,x2,y2) in linelist:
            newlinelist.append( (x1, self.ctx.page_height-y1, x2, self.ctx.page_height-y2) )
        _Canvas.lines(self, newlinelist)
