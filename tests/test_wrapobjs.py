# -*- coding: utf-8 -*-


from docreport.wrapobjs import Text, Composition
from docreport.localreportlab import MyParagraph
from docreport.pagedata import styleN, mm
from docreport.filters import Filter
from docreport.filters import T
from reportlab.lib.colors import red
from docreport import borders
from utils import pdfFileCanvas
from docreport.borders import hborders, vborders
from docreport.context import addStyle

def _test_page(ctx):
    # class TmpFilter(Filter):
    #     def text(self, txt):
    #         txt = super(TmpFilter, self).text(txt)
    #         txt ="* "+txt
    #         return txt


    def newPage(title):
        ctx.canvas.showPage()
        Text(title, 200*mm, ctx=ctx).drawAt(20*mm, 10*mm)

    def l(canvas, x1,y1, obj):
        thikness = 3

        x2 = x1 + obj.width
        y2 = y1 + obj.height
        canvas.line( x1-thikness,y1, x1,y1) # -
        canvas.line( x1,y1-thikness, x1,y1) # |

        canvas.line( x2,y1, x2+thikness,y1)  # -
        canvas.line( x2,y1, x2,y1-thikness)  # |

        canvas.line( x1-thikness,y2, x1, y2) # -
        canvas.line( x1,y2, x1,y2+thikness)  # |

        canvas.line( x2,y2, x2+thikness,y2)
        canvas.line( x2,y2, x2, y2+thikness)

    #################### PAGE 1
    Text("", 200*mm, ctx=ctx).drawAt(20*mm, 10*mm )

    xx=10; min_height=None;

    yy=20; ll=50;
    #
    Text("[{}:{},{}] Text Test 1".format(xx,yy,ll), ll*mm, ctx=ctx.clone(border_cb=l),
            min_height=min_height).drawAt(xx*mm, yy*mm)

    yy=30; ll=80
    Text("[{}:{},{}] 1 riga breve".format(xx,yy,ll), ll*mm, ctx=ctx.clone(border_cb=l),
            min_height=min_height).drawAt(xx*mm, yy*mm)

    yy=50; ll=50

    Text("[{}:{},{}] 1 riga spezzata a metà".format(xx,yy,ll),
        ll*mm, ctx=ctx.clone(border_cb=l), min_height=min_height).drawAt(xx*mm, yy*mm)

    yy=70; ll=40; min_height=50;

    Text("[{}:{},{}] No <br/>escape <br/>    min 40".format(xx,yy,ll), ll*mm,
        ctx=ctx.clone(border_cb=l),
            min_height=min_height).drawAt(xx*mm, yy*mm)

    xx=150
    yy=70; ll=60; min_height=50;
    padding = 5*mm


    with ctx.up(border_cb=l, x_padding=padding, y_padding=padding, style=addStyle(textColor=red)):
        w = Text("[{}:{},{}] Padding".format(xx,yy,ll), ll*mm, ctx=ctx.clone(border_cb=l),
                min_height=min_height,
            ).drawAt(xx*mm, yy*mm)

    ctx.canvas.line(xx*mm-10,yy*mm, xx*mm,yy*mm)
    ctx.canvas.line(xx*mm-10,yy*mm+padding, xx*mm,yy*mm+padding)
    ctx.canvas.line(xx*mm-10,yy*mm+padding+w._pb_height, xx*mm,yy*mm+padding+w._pb_height)
    ctx.canvas.line(xx*mm-10,yy*mm+padding*2+w._pb_height, xx*mm,yy*mm+padding*2+w._pb_height)


    # tmpcontext = default_context.copy()
    # tmpcontext['filter_cls'] = TmpFilter
    # Text("[{}:{},{}] Escape\n    min 40".format(xx,yy,ll), ll*mm,
    #         min_height=min_height, border_cb=l, ctx=c.ctx).drawAt(xx*mm, yy*mm)


    ## composition
    w1 = Text("W1", 30*mm, ctx=ctx.clone(border_cb=l))
    w2 = Text("W2", 40*mm, ctx=ctx.clone(border_cb=l))
    w3 = Text("W3", 50*mm, ctx=ctx.clone(border_cb=l))

    h1 = Composition(ctx)
    h1.hAdd(w1)
    h1.hAdd(w2)
    h1.hAdd(w3)
    h1.drawAt(30*mm, 120*mm)

    h2 = Composition(ctx)\
        .hAdd(w1)\
        .hAdd(w2)\
        .hAdd(w3)
    h2.drawAt(30*mm, 150*mm)

    v1 = Composition(ctx)\
        .vAdd(h1)\
        .vAdd(Text("Single field", 65*mm,ctx=ctx))\
        .vAdd(h2)\
        .drawAt(30*mm, 200*mm)

    newPage("Test Markup System") ###################################

    text0 = Text('Test1', 30*mm, ctx=ctx).drawAt(20*mm, 30*mm)
    text1 = Composition(ctx)\
        .vAdd(Text(["This ", "is ", "a " , "list"], 120*mm, ctx=ctx.clone(style=addStyle(textColor=red, fontName='Helvetica-Bold'))))\
        .vAdd(Text(["To be wrapped (default escaping): ", '<b>example</b>'], 120*mm, ctx=ctx))\
        .vAdd(Text(["No wrap: ", T.N['<b>example</b>']], 120*mm, ctx=ctx))\
        .vAdd(Text(["Tag wrap: ", T.b['example']], 120*mm, ctx=ctx))

    text1.drawAt(20*mm, 30*mm + text0.height)

    newPage("Borders") ###################################

    Composition(ctx)\
        .hAdd(Text("Col1", 15*mm, ctx=ctx))\
        .hAdd(Text("Col2", 15*mm, ctx=ctx))\
        .hAdd(Text("Col3", 15*mm, ctx=ctx))\
        .drawAt(20*mm, 40*mm)

    Composition(ctx.clone(border_cb=vborders))\
        .vAdd(Text("row1", 15*mm, ctx=ctx.clone(border_cb=hborders)))\
        .vAdd(Text("row2", 15*mm, ctx=ctx.clone(border_cb=hborders)))\
        .vAdd(Text("row3", 15*mm, ctx=ctx.clone(border_cb=hborders)))\
        .drawAt(20*mm, 60*mm)

    newPage("Relative position")  ############################
    #c.showPage()  ######################################
    # Text("Relative position (to the last draw point)", 200*mm).drawAt(20*mm, 20*mm )

    #
    w1 = Text("Text", 15*mm, min_height=30*mm, ctx=ctx)
    w2 = Text("Bottom", 15*mm, ctx=ctx.clone(border_cb=borders.borderbox))
    cc = Composition(ctx.clone(border_cb=borders.borderbox))
    cc.hAdd(w1)
    cc.add(w2, 2*mm, cc.height-w2.height)
    cc.drawAt(20*mm, 40*mm)
    # w3 = Text("Bottom", 15*mm)
    # w1.drawAt(30*mm, 40*mm)
    # w2.drawOn(w1, x=w1.width)
    # w3.drawOn(w2, y=w2.height)

    # w1 = Text("Center", 15*mm)
    # w2 = Text("Right", 15*mm)
    # w3 = Text("Bottom", 15*mm)
    # w2.drawOn(w1, x=w1.width)
    # w3.drawOn(w2, y=w2.height)
    # w1.drawAt(30*mm, 70*mm)

    newPage("Composition")  ############################
    cc = Composition(ctx)\
        .hAdd(Text("H1", 15*mm, ctx=ctx))\
        .hAdd(Text("H2", 20*mm, ctx=ctx))\
        .hAdd(Text("H3", 30*mm, ctx=ctx))
    cc.drawAt(20*mm, 40*mm)

    cc2 = Composition(ctx)\
        .hAdd(Text("R1", 15*mm, ctx=ctx))\
        .vAdd(cc)\
        .vAdd(Text("R3", 15*mm, ctx=ctx))
    cc2.drawAt(20*mm, 60*mm)

    newPage("Composition2")  ############################
    #
    # 'error','overflow','shrink','truncate'
    #
    with ctx.up(border_cb=borders.borderbox):
        Text('One row', width=30*mm, height=30*mm,
            mode='overflow',ctx=ctx).drawAt(10*mm, 30*mm)
        Text('One row', width=30*mm, min_height=30*mm,
            mode='overflow',ctx=ctx).drawAt(10*mm, 70*mm)
        w=Text('One row '*40 , width=30*mm,  height=30*mm,
            mode='overflow',ctx=ctx).drawAt(10*mm, 110*mm)

        Text('One row '*40 , width=30*mm, height=30*mm,
            mode='shrink',ctx=ctx).drawAt(50*mm, 30*mm)

        Text('One row '*40 , width=30*mm, height=30*mm,
            mode='truncate',ctx=ctx).drawAt(90*mm, 30*mm)

        Text(u'& prova &&' , width=30*mm, height=30*mm,
            mode='truncate',ctx=ctx).drawAt(10*mm, 250*mm)


    with ctx.up(border_cb=borders.borderbox, style=addStyle(alignment=1)):
        Text(u'.      CENTRATO      -' , width=190*mm, height=30*mm, ctx=ctx, ).drawAt(10*mm, 270*mm)

    with ctx.up(border_cb=borders.borderbox):
        Text(T.N[u'  SINISTRA    NNNN           -'.replace(' ', "&nbsp;")],
                 width=190*mm, height=30*mm, ctx=ctx, ).drawAt(10*mm, 265*mm)
        Text(u'  SINISTRA    NNNN           -',
                 width=190*mm, height=30*mm, ctx=ctx, ).drawAt(10*mm, 270*mm)

    # w.name='aa'
    # print (w,22222222222222222)

    # root.layout().row().cell() => Text
    # root.layout().row().cell() => Text

if __name__=='__main__':
    # testing page
    with pdfFileCanvas() as ctx:
        _test_page(ctx)
