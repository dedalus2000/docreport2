# -*- coding: utf-8 -*-

from docreport.wrapobjs import WrappableInterface, Text, Composition
from docreport.borders import hborders, vborders, table_borders,borderbox
from docreport.thepage import MyPage #, RowsUtil
from docreport.pagedata import mm
from docreport.filters import T
from utils import pdfFileCanvas
from reportlab.lib.pagesizes import A4
import random
from docreport.context import ContextA4
from reportlab.lib.colors import Color
import os
from reportlab.platypus import Image
from reportlab.lib.colors import red
from docreport.context import addStyle

def _test_page(ctx):
    def newPage(title, firstpage=False):
        if not firstpage:
            ctx.canvas.showPage()
        Text(title, 200*mm, ctx=ctx).drawAt(20*mm, 10*mm)
        #Text("ciao", 500*mm, ctx=ctx).drawAt(20*mm, 10*mm)


        #  c.drawImage(ss, 104.197, 196.81, width=None, height=None, mask='auto')
        if 0:
            ctx.canvas.saveState()
            ctx.canvas.scale(20,20)
            fs = ctx.canvas._fontsize
            ctx.canvas.translate(1, -fs/1.2)  # canvas._leading?
            ctx.canvas.rotate(45)
            ctx.canvas.translate(20,10)  # canvas._leading?
            red50transparent = Color( 100, 0, 0, alpha=0.3)
            ctx.canvas.setFillColor(red50transparent)
            ctx.canvas.drawString(0,0, "Draft")
            ctx.canvas.restoreState()


        #
        #from reportlab.lib.enums import TA_LEFT, TA_CENTER

        #  l'immagine è 364x310 punti
    #     kk = 1.2
    #     im = Image(os.path.join(os.path.dirname(__file__), 'draft.png'), 364*kk,310*kk)
    #     print  999,dir(im), im._width, im._height
    #     im.hAlign = 'LEFT'
    #     xx,yy= im.wrap(160*mm, 160*mm)
    #    # print (xx/mm, yy/mm)
    #     im.drawOn(ctx.canvas, A4[0]/2.-xx/2., A4[1]/2.-yy/2.)


        kk = 1.2
        from PIL import Image
        im = Image.open('draft.png')

        xx, yy = im.size
        ctx.canvas.drawImage(
            os.path.join(os.path.dirname(__file__), 'draft.png'),
            ctx.page_width/2.-xx*kk/2., ctx.page_height/2.-yy*kk/2.,
            #
            #xx*kk,yy*kk,mask='auto'
            mask='auto'
            # mask=[0,2,40,42,136,139]
        )

    #c.ctx = ContextA4(cols_widths = [40*mm,20*mm,40*mm])

    # rr = RowsUtil(ctx=ctx)
    # rr.addRowValues(['col1', 'col2', 'col3'])
    # rr.addRowValues(['col4 è molto grossa ma molto molto', 'col5', 'col6'])
    # rr.addRowValues(['col7', 'col8', 'col9'])
    # rr.addRowValues(['col7', 'col8', 'col9']) #, cols_widths=False)
    # rr.addRowValues(['col7', 'col8', 'col9']) #, cols_widths=[20*mm,None,None])
    # rr.drawAt(10*mm, 30*mm)

    # a,b = rr.splitByHeight(rr.paths[0].height+1)
    # a.drawAt( 10*mm, 140*mm)
    # b.drawAt( 10*mm, 180*mm)

    # newPage("Page 2: Borders") ##################  With borders
    # ctx.cols_widths=[40*mm,20*mm,40*mm]
    #c.ctx.border_cb=vborders

    # rr = RowsUtil(ctx=ctx)
    # rr.addRowValues(['col1', 'col2', 'col3'])
    # rr.addRowValues(['col4 è molto grossa ma molto molto', 'col5', 'col6'])
    # rr.addRowValues(['col7', 'col8', 'col9'])
    # rr.drawAt( 10*mm, 30*mm)

    # rr = RowsUtil(ctx=ctx) #, border_cb=table_borders)
    # rr.addRowValues(['col1', 'col2', 'col3'])
    # rr.addRowValues(['col4 è molto grossa ma molto molto', 'col5', 'col6'])
    # rr.addRowValues(['col7', 'col8', 'col9'])
    # rr.drawAt( 10*mm, 150*mm)
    from reportlab.lib.enums import TA_LEFT, TA_RIGHT
    newPage("Page A: Page", firstpage=True) #####################################################
    mp = MyPage(ctx.canvas) #, rows=dict(border_cb=table_borders))
    mp.Text("ROSSO dict", 150*mm, ctx=dict(style=addStyle(textColor=red))).drawAt(60,60)
    mp.Text("ROSSO dict2", 150*mm, ctx=dict(style=addStyle(alignment=TA_RIGHT))).drawAt(60,60)

    mp.Text(
        T.font(fontSize=6)["  ", "title" ], 400, ctx=dict(border_cb=borderbox),
    ).drawAt(60,80)
    mp.Text(
        T.para(align='right')['ciao'], 400, ctx=dict(border_cb=borderbox)
    ).drawAt(60,100)

    mp.ctx.cols_widths = [40*mm,20*mm,40*mm]
    mp.page_height = A4[1]
    for ii in range(35):
#        mp.addRowValues(['col{}'.format(ii), 'col2', 'col3'])
        mp.addRowValues(['\n'.join(['col1']*random.randint(2,2)) , 'col2', 'col3 [%s]'%ii])

    newPage("Page B: inherited page") #####################################################

    class Page(MyPage):
        def manage_ctx(self, ctx):
            print ("ahahah", ctx)
        def on_init_page(self, rows):
            rect_width = self.ctx.page_width-20*mm
            self.canvas.rect(10*mm,10*mm,rect_width,20*mm)
            self.Text('Page %s' % self.curpage, 20*mm).drawAt(10*mm, 20*mm)

    # class Page2(Page):
    #     ctx_defaults = {
    #         'bb':1
    #     }

    mp = Page(ctx.canvas, cols_widths=[40*mm,20*mm,40*mm]) #, rows=dict(border_cb=table_borders))
    for ii in range(16):
        # st = {}
        # if ii%2==0:
        #     st = dict(style=addStyle(color=red))
        mp.addRowValues(fields=['\n'.join(['col1']*random.randint(1,3)) , 'col2', 'col3'])




    # im = Image(, )
    # print  999,dir(im), im._width, im._height
    # im.hAlign = 'LEFT'
    # xx,yy= im.wrap(160*mm, 160*mm)
    # # print (xx/mm, yy/mm)
    # im.drawOn(ctx.canvas, A4[0]/2.-xx/2., A4[1]/2.-yy/2.)
    Text("prova", 700, ctx).drawAt(30,30)
    ctx.canvas.showPage()
    Text("prova", 700, ctx).drawAt(30,30)

if __name__=='__main__':
    # testing page
    with pdfFileCanvas() as ctx:
        _test_page(ctx)
