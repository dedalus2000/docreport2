
from __future__ import print_function
from docreport.localreportlab import MyCanvas
from contextlib import contextmanager
import subprocess
import tempfile
import sys
import os
from reportlab.lib.pagesizes import A4
from docreport.filters import Filter
from docreport.context import ContextA4


@contextmanager
def pdfFileCanvas(fname=None, open=True):
    if fname is None:
        ff = tempfile.NamedTemporaryFile(suffix=".pdf", delete=False)
        # ff.close()
        # fname = ff.name
        fname = ff
    else:
        if not fname.endswith('.pdf'):
            fname = "%s.pdf" % fname

    ctx = ContextA4(x_padding=2, y_padding=2)

    c = MyCanvas(fname, ctx=ctx)
    def writePageNumber(p_number, p_count):
        self = c
        self.setFont("Helvetica", 7)
        self.drawRightString(self.ctx.page_width-40, 40,
            "Page %(this)i of %(total)i" % {
                'this': self._pageNumber+1,
                'total': len(self._codes),
            }
        )
    c.writePageNumber = writePageNumber
    assert c.ctx
    assert ctx.canvas
    yield ctx

    c.save()

    #print (fname)  # you can use okular `... |tail -n 1`

    #if sys.platform.startswith('freebsd'):
        # FreeBSD-specific code here...
    if sys.platform.startswith('linux'):
        subprocess.call(["xdg-open", fname.name])
    elif sys.platform == "darwin":
        subprocess.call(["open", fname.name])
    elif sys.platform in ('win32', 'cygwin'):
        os.startfile(fname.name)
