# -*- coding: utf-8 -*-


from docreport.wrapobjs import Text, Composition
from docreport.localreportlab import MyParagraph
from docreport.pagedata import styleN, mm
from docreport.filters import Filter
from docreport.filters import T
from reportlab.lib.colors import red
from docreport import borders
from utils import pdfFileCanvas
from docreport.borders import hborders, vborders
from docreport.context import addStyle


def _test_page(ctx):
    # class TmpFilter(Filter):
    #     def text(self, txt):
    #         txt = super(TmpFilter, self).text(txt)
    #         txt ="* "+txt
    #         return txt


    rr = 20

    print ("FONT NAME", ctx.canvas._fontname, "  Size", ctx.canvas._fontsize)

    #for fontname in ('RobotoBlack', 'RobotoBoldItalic', 'RobotoBold'):
        #ctx.canvas.setFont(fontname, 20)
    Text(["Robo", " bās ", T.b['bold']], 55*mm,ctx=ctx).drawAt(20*mm, rr*mm)
    rr +=15



if __name__=='__main__':
    # testing page
    with pdfFileCanvas() as ctx:
        _test_page(ctx)
