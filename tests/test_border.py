# -*- coding: utf-8 -*-


from docreport.wrapobjs import Text, Composition
from docreport.localreportlab import MyParagraph
from docreport.pagedata import styleN, mm
from docreport.filters import Filter
from docreport.filters import T
from reportlab.lib.colors import red
from docreport import borders
from utils import pdfFileCanvas
from docreport.borders import hborders, vborders
from docreport.context import addStyle


def _test_page(ctx):
    # class TmpFilter(Filter):
    #     def text(self, txt):
    #         txt = super(TmpFilter, self).text(txt)
    #         txt ="* "+txt
    #         return txt

    def columns(n=3):
        return Composition(ctx=ctx)\
            .hAdd(Text("col1", 15*mm, ctx=ctx.clone(border_cb=borders.borderbox)))\
            .hAdd(Text("col2", 15*mm, ctx=ctx.clone(border_cb=borders.borderbox)))\
            .hAdd(Text("col3", 15*mm, ctx=ctx.clone(border_cb=borders.borderbox)))\
            .drawAt(20*mm, 60*mm)


    Composition(ctx.clone(border_cb=borders.borderbox))\
        .vAdd(columns())\
        .vAdd(columns())\
        .vAdd(columns())\
        .drawAt(20*mm, 60*mm)


if __name__=='__main__':
    # testing page
    with pdfFileCanvas() as ctx:
        _test_page(ctx)
